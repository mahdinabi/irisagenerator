const Generator = require('yeoman-generator')
const fs = require('fs') //used for reading and writing files
const prettier = require('prettier') //used for formatting the generated code
const items = require('./items.json') //main items which exist in the form
const specialImports = require('./specialImports.json') //imports needed for components
const ejs = require('ejs') //used for generating code from template files
const chalk = require('chalk') // used for colorful console outputs

module.exports = class extends Generator {
  // imports section

  constructor(args, opts) {
    super(args, opts)
    this.option('babel')
    this.path = this.templatePath('..')
    this.imports = "import React from 'react'"
    this.template = ''
    this.log(this.path)
    this.log(fs.readdirSync(this.path + '/sample'))
  }

  initializing() {
    this.log(
      chalk.yellow('\n\nhi! ') +
        'thanks for using ' +
        chalk.bgBlue('Irisa Generator') +
        '!'
    )

    // getting item types for adding imports to the generated code
    const types = [...new Set(items.map(data => data.type))]
    types.forEach(type => {
      if (type in specialImports) {
        specialImports[type].forEach(importItem => {
          //searching if this item needs special imports
          if (!this.imports.includes(importItem))
            this.imports += '\n' + importItem
        })
      } else {
        this.imports += `\nimport ${type} from '@material-ui/core/${type}'`
      }
    })
    this.template = fs.readFileSync(this.templatePath('form.ejs'), 'utf-8')
  }

  writing() {
    this.log('generating' + chalk.cyan(' client') + '... ')

    const files = fs.readdirSync(this.templatePath('../sample/'))
    files.forEach(file => {
      this.fs.copy(this.templatePath('../sample/' + file), './client/' + file)
    })

    //rendering the output code from the input 'form.ejs' file
    const generated = ejs.render(
      this.template,
      {
        items,
        fs
      },
      { filename: this.templatePath('form.ejs') }
    )

    //formatting the generated(rendered) code
    const formatted = prettier.format(generated, {
      semi: false,
      parser: 'babel',
      jsxSingleQuote: true
    })

    //writing file to output dir
    fs.writeFileSync('./client/src/Form.jsx', this.imports + '\n' + formatted)

    this.log(chalk.green('Done!'))
  }
}
