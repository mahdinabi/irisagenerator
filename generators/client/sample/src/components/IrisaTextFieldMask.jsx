import React from 'react'
//import {withStyles,TextField} from '@material-ui/core';
//import {styles} from '../../assets/jss/style'
import PropTypes from 'prop-types'
import MaskedInput from 'react-text-mask'
import TextField from '@material-ui/core/TextField'

export default IrisaTextFieldMask

function IrisaTextFieldMask(props) {
  return (
    <MaskedInput
      mask={props.mask}
      render={(ref, props) => {
        const {
          classes,
          label,
          name,
          fullWidth,
          rows,
          value,
          multiline,
          onChange,
          ...other
        } = props
        return (
          <TextField
            {...props}
            name={name}
            label={label}
            inputRef={ref}
            value={value === null ? '' : value}
            onChange={onChange}
            fullWidth={fullWidth}
            rows={rows}
            multiline={multiline}
          />
        )
      }}
    />
  )
}

// function IrisaTextFieldMask(props) {
//   return (
//     <TextField
//       InputProps={{
//         inputComponent: TextMaskCustom,
//         value: props.value,
//         onChange: props.onChange
//       }}
//     />
//   )
// }

// const TextMaskCustom = props => {
//   const { inputRef, ...other } = props

//   return (
//     <MaskedInput
//       {...other}
//       ref={inputRef}
//       mask={[
//         '(',
//         /[1-9]/,
//         /\d/,
//         /\d/,
//         ')',
//         ' ',
//         /\d/,
//         /\d/,
//         /\d/,
//         '-',
//         /\d/,
//         /\d/,
//         /\d/,
//         /\d/
//       ]}
//       placeholderChar={'\u2000'}
//       showMask
//     />
//   )
// }

IrisaTextFieldMask.propTypes = {
  //classes: PropTypes.object.isRequired,
}
