import React from 'react'
import logo from './logo.svg'
import { createMuiTheme, makeStyles } from '@material-ui/core/styles'
import { ThemeProvider } from '@material-ui/styles'
import Form from './Form'
import './App.css'

const theme = createMuiTheme({
  direction: 'rtl'
})
const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'column',
    margin: theme.spacing(20),
    marginLeft: theme.spacing(70),
    marginRight: theme.spacing(70)
  }
}))

function App() {
  const [irisaTextFieldMask, setIrisaTextFieldMask] = React.useState({
    value: ''
  })

  return (
    <ThemeProvider theme={theme}>
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
        </header>
        <body>
          <Form />
        </body>
      </div>
    </ThemeProvider>
  )
}

export default App
